Run the included ACBR.exe (Note: You may see a warning from Microsoft Defender SmartScreen indicating that this program is an unrecognized app, in which case you will need to click "More info" and then "Run anyway" in order to run the program.)
The program will launch as a console application and prompt you to maximize the resulting console window. You can then add/remove racers and start a race!
