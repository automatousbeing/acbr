/*********************************************************
*
* Title: Announcer.h
* Author: ACBR Team
* Description: Header file for the Announcer singleton, which
* manages these message marquee display underneath a race
* currently in progress
*
**********************************************************/

#pragma once

#include <string.h>
#include <vector>

using std::string;
using std::vector;


class Announcer
{
static Announcer* instance;

public:
	static Announcer* getInstance();

	vector<string> getAnnouncements(int amount);
	void addAnnouncement(string announcement);
	void clearAnnouncements();

private:
	Announcer();
	~Announcer();

	vector<string> announcements;
};

