#include "pch.h"
#include "Race.h"
#include "Announcer.h"

#include <iostream>
#include <iomanip>
#include <string>

#include "PressEnterHalt.h"

#include <random>
#include "RacerFactory.h"

using std::cout;
using std::endl;

Race::Race()
{
	display = new RaceDisplayTerminal();

	racers.clear();
}

Race::~Race()
{
	// Clean up racers and obstacles
}

void Race::resetRace() {
	// Reset starting positions
	for (int i = 0; i < racers.size(); ++i) {
		Racer *racer = racers[i];
		racer->setPos(0.0);
		racer->slowByPercent(1.0f);
	}

	// Randomly generate 1-3 obstacles
	obstacles.clear();
	int obstacleCount = 1 + rand() % 3;
	for (int i = 0; i < obstacleCount; ++i) {
		Obstacle *obs = new Obstacle();
		obstacles.push_back(obs);
	}

	leader = NULL;

	initLastCollisionPosPerRacer();
}

bool Race::collidingWithObstacle(Racer* racer) {
	for (Obstacle* obs : obstacles) {
		int obsRoundedDistFromStart = obs->getPos()->getRoundedDistFromStart();
		if
		(
			lastCollisionPosPerRacer[racer] != obsRoundedDistFromStart
			&& racer->getPos()->equals(obs->getPos())
		)
		{
			lastCollisionPosPerRacer[racer] = obsRoundedDistFromStart;
			return true;
		}
	}
	return false;
}

void Race::initLastCollisionPosPerRacer()
{
	for (Racer* racer : racers)
	{
		if (lastCollisionPosPerRacer.find(racer) == lastCollisionPosPerRacer.end())
		{
			lastCollisionPosPerRacer.insert({ racer, -1 });
		}
		else
		{
			lastCollisionPosPerRacer[racer] = -1;
		}
	}
}

void Race::resetAnnouncements()
{
	Announcer::getInstance()->clearAnnouncements();
	bool allRacersHaveEngines = true;
	for (Racer* racer : racers)
	{
		if (!racer->hasEngine())
		{
			allRacersHaveEngines = false;
			break;
		}
	}
	string startingAnnouncement =
		allRacersHaveEngines ?
		"Racers, start your engines!" :
		"Racers, start your... um GET READY TO RACE!";
	Announcer::getInstance()->addAnnouncement(startingAnnouncement);
}

int Race::racerCount() {
	return racers.size();
}

vector<Racer*> Race::getRacers() {
	return racers;
}

void Race::addPlayer(RACER_TYPE type) {
	// Not implementing?
}

void Race::addAI(RACER_TYPE type) {
	racers.push_back(RacerFactory::makeRacer(type));
}

void Race::removeRacer(int index) {
	if (index < racers.size()) {
		racers.erase(racers.begin() + index);
	}
}

void Race::startRace() {
	resetRace();
	resetAnnouncements();

	cout << "----------------------------------------------------------------------" << endl;
	cout << "Race ready to start!" << endl;

	display->drawScreen(length, racers, obstacles);

	cout << "Press enter to start the race!" << endl;
	cout << "----------------------------------------------------------------------" << endl;
	forceStop();
}

// Each frame, step the racers forward with their step frame functions, check for collisions against
// obstacles, and generate any necessary announcements for new events
void Race::stepFrame() {	
	Announcer *a = a->getInstance();
	if (leader == NULL && racers.size() > 0) {
		leader = racers[0];
	}
	for (int i = 0; i < racers.size(); ++i) {
		Racer *racer = racers[i];
		if (leader != NULL && (racer->getPos()->getDistFromStart() > leader->getPos()->getDistFromStart())) {
			leader = racer;
			string announcement = racer->getName() + " has taken the lead!";	
			a->addAnnouncement(announcement);
		}
		racer->stepFrame();
		if (collidingWithObstacle(racer))
		{
			string announcement = racer->getName() + " collided with an obstacle!";	
			a->addAnnouncement(announcement);
			racer->slowByPercent(OBSTACLE_SLOW_BY_PERCENT);
		}
	}
}

void Race::renderFrame() {
	system("CLS");
	display->drawScreen(length, racers, obstacles);
}

bool Race::raceComplete() {
	for (int i = 0; i < racers.size(); ++i) {
		Racer *racer = racers[i];
		if (racer->getPos()->getDistFromStart() >= length) {
			return true;
		}
	}

	return false;
}
