/*********************************************************
*
* Title: ACBRGame.h
* Author: ACBR Team
* Description: Header file for the ACBRGame class which handles
* a single instance of a match, it's race, and all helper 
* utilities
*
**********************************************************/

#pragma once

#include "Race.h"

#include <string>
#include <vector>

using std::string;
using std::vector;

class ACBRGame
{
public:
	ACBRGame();
	~ACBRGame();

	void initialize();

private:
	const int menu_exit_index = 5;
	const int racer_type_count = 4;
	const int max_racers = 4;

	string borderString;

	Race *race;

	void printMenu();
	void printRacerInfo();
	void handleRacerRemoval();
	bool handleMenuSelection(int in);
	void handleAddAI();

	void play();
	void gameLoop();
};

