#include "pch.h"
#include "Horse.h"
#include "RacerType.h"

// Define the type and stats.
Horse::Horse() : Racer(RACER_TYPE::HORSE) {
	this->maxSpeed = 0.46f;
	this->acceleration = 0.028f;
}

// Get a name which can be displayed to the user.
string Horse::getName() {
	return "Horse";
}

// Return whether or not this racer has an engine.
bool Horse::hasEngine() {
	return false;
}