#include "pch.h"
#include "Announcer.h"

Announcer *Announcer::instance = 0;

// Singleton patterns insures that via the getInstance method, only one 
// instance of the class will be created
Announcer* Announcer::getInstance() {
	if (!instance) {
		instance = new Announcer;
	}

	return instance;
}

Announcer::Announcer()
{
}


Announcer::~Announcer()
{
}

// Use the amount given by the rendering code to create a reverse sorted list
// of announcements to display
vector<string> Announcer::getAnnouncements(int amount) {
	vector<string> returnAnnouncements;

	vector<string> reverse = announcements;
	std::reverse(reverse.begin(), reverse.end());

	int size = reverse.size() < amount ? reverse.size() : amount;

	for (int i = 0; i < size; ++i) {
		returnAnnouncements.push_back(reverse[i]);
	}

	return returnAnnouncements;
}

void Announcer::addAnnouncement(string announcement) {
	announcements.push_back(announcement);
}

void Announcer::clearAnnouncements() {
	announcements.clear();
}
