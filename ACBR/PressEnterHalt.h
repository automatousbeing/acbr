/*********************************************************
*
* Title: PressEnterHalt.h
* Author: ACBR Team
* Description: Header file for the forceStop util, which is
* just a mechanism for being able to draw to the screen and 
* halt execution, waiting for the user to press enter after
* they've read what's printed.
*
**********************************************************/

#pragma once

void forceStop();