#include "pch.h"
#include "RacetrackPos.h"
#include <math.h>

// Set the initial distance from start to 0 by default.
RacetrackPos::RacetrackPos() {
	distFromStart = 0.0f;
}

// Return the current distance from the starting line of the race track.
float RacetrackPos::getDistFromStart()
{
	return distFromStart;
}

// Set the current distance from the starting line of the race track.
void RacetrackPos::setDistFromStart(float value)
{
	distFromStart = value;
}

// Get the distance from the starting line rounded to the nearest whole number.
int RacetrackPos::getRoundedDistFromStart()
{
	return int(round(distFromStart));
}

// Compare positions based on their rounded distances from the starting line.
bool RacetrackPos::equals(RacetrackPos* other)
{
	return this->getRoundedDistFromStart() == other->getRoundedDistFromStart();
}

// Add position by adding their distance from the starting line.
RacetrackPos& RacetrackPos::operator +=(float rhs)
{
	this->distFromStart += rhs;
	return *this;
}
