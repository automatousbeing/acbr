#include "pch.h"
#include "RaceDisplayTerminal.h"
#include "Announcer.h"

#include <math.h>

#include <vector>
#include <map>
#include <iomanip>
#include <iostream>

using std::vector;
using std::map;
using std::cout;
using std::endl;

RaceDisplayTerminal::RaceDisplayTerminal() {

}

RaceDisplayTerminal::~RaceDisplayTerminal() {

}

void RaceDisplayTerminal::clearDisplay() {

} 

// The render function works in a scanline fashion, printing every character for every position, in each row
// for the entire screen, checking at each point whether a racer, an obstacle, or a piece of track needs to
// be laid down.  After printing the track, the announcements are rendered underneath, currently displaying
// a max of 10
void RaceDisplayTerminal::drawScreen(int length, vector<Racer*> racers, vector<Obstacle*> obstacles) {
	map<int, Obstacle*> obstaclePositions;

	for (int i = 0; i < obstacles.size(); ++i) {
		Obstacle *obs = obstacles[i];
		obstaclePositions[obs->getPos()->getRoundedDistFromStart()] = obs;
	}

	for (int i = 0; i < racers.size(); ++i) {
		for (int j = 0; j < length; ++j) {
			Racer *racer = racers[i];

			if (j == racer->getPos()->getRoundedDistFromStart()) {
				displayRacer(racer);
			}
			else if (obstaclePositions.find(j) != obstaclePositions.end()) {
				Obstacle *obs = obstaclePositions[j];
				displayObstacle(obs);
			}
			else {
				displayTrack();
			}
		}
		cout << "\n" << "\n";
	}

	Announcer *a = a->getInstance();
	vector<string> announcements = a->getAnnouncements(10);
	for (int i = 0; i < announcements.size(); ++i) {
		cout << announcements[i] << endl;
	}

	cout << endl;
}

void RaceDisplayTerminal::displayObstacle(Obstacle *obstacle) {
	cout << "0";
}

void RaceDisplayTerminal::displayRacer(Racer *racer) {
	switch (racer->getType()) {
	case HORSE:
		cout << "H ";
		break;
	case CAR:
		cout << "C ";
		break;
	case SPACESHIP:
		cout << "S ";
		break;
	case AIRPLANE:
		cout << "A ";
		break;
	}
}

void RaceDisplayTerminal::displayTrack() {
	cout << "_.";
}