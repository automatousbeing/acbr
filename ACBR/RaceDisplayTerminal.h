/*********************************************************
*
* Title: RaceDisplayTerminal.h
* Author: ACBR Team
* Description: Header file for the Terminal implementation
* of the RaceDisplay interface
*
**********************************************************/

#include "RaceDisplay.h"

#include <vector>

using std::vector;

class RaceDisplayTerminal : RaceDisplay {
public:
	RaceDisplayTerminal();
	~RaceDisplayTerminal();

	void clearDisplay();
	void drawScreen(int length, vector<Racer*> racers, vector<Obstacle*> obstacles);
	void displayObstacle(Obstacle *obstacle);
	void displayRacer(Racer *racer);
	void displayTrack();
};
