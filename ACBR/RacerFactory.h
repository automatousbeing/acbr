/*********************************************************
*
* Title: RacerFactory.h
* Author: ACBR Team
* Description: Header file for the RacerFactory class which
* handles the creation of instances of subclasses of the
* Racer class based on a given type.
*
**********************************************************/

#pragma once
#include "Racer.h"

class RacerFactory
{
public:
	static Racer* makeRacer(RACER_TYPE type);
};