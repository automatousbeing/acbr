// ACBR.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
//

#include "pch.h"
#include <iostream>
#include "ACBRGame.h"

int main()
{
	//Game entry point
	ACBRGame acbrInstance = ACBRGame();
	acbrInstance.initialize();
}
