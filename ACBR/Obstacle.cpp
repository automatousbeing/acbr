#include "pch.h"
#include "Obstacle.h"

#include <random>
#include <time.h>

Obstacle::Obstacle()
{
	// Randomly generated obstacles position, with a random tenths place value, and a
	// lower bound of 10.0
	pos = new RacetrackPos();
	pos->setDistFromStart(rand()%25 + (rand()%10)/10.0 + 10.0);
}

Obstacle::~Obstacle()
{
}

RacetrackPos* Obstacle::getPos() {
	return pos;
}

void Obstacle::setPos(float posIn) {
	pos->setDistFromStart(posIn);
}
