/*********************************************************
*
* Title: Car.h
* Author: ACBR Team
* Description: Header file for the Car class which is
* is a Racer, so it is a playable character. It has a
* moderate max speed and slightly higher acceleration.
*
**********************************************************/

#pragma once

#include "Racer.h"
#include <string>

using std::string;

class Car : public Racer
{
public:
	Car();
	string getName();
	bool hasEngine();
};