/*********************************************************
*
* Title: Obstacle.h
* Author: ACBR Team
* Description: Header file for the Obstacle object class
*
**********************************************************/

#pragma once
#include "RacetrackPos.h"

class Obstacle
{
public:
	Obstacle();
	~Obstacle();

	RacetrackPos* getPos();
	void setPos(float posIn);
private:
	RacetrackPos* pos;
};

