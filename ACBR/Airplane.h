/*********************************************************
*
* Title: Airplane.h
* Author: ACBR Team
* Description: Header file for the Airplane class which is
* is a Racer, so it is a playable character. It has a
* moderate acceleration and slightly higher max speed.
*
**********************************************************/

#pragma once

#include "Racer.h"
#include <string>

using std::string;

class Airplane : public Racer
{
public:
	Airplane();
	string getName();
	bool hasEngine();
};