#include "pch.h"
#include "Airplane.h"
#include "RacerType.h"

// Define the type and stats.
Airplane::Airplane() : Racer(RACER_TYPE::AIRPLANE) {
	this->maxSpeed = 0.64f;
	this->acceleration = 0.0196;
}

// Get a name which can be displayed to the user.
string Airplane::getName() {
	return "Airplane";
}

// Return whether or not this racer has an engine.
bool Airplane::hasEngine() {
	return true;
}