/*********************************************************
*
* Title: Race.h
* Author: ACBR Team
* Description: Header file for the Race class, which manages
* a single race instance, holding onto the Racer objects, as
* well as calculating the game loop
*
**********************************************************/

#pragma once

#include "RaceDisplayTerminal.h"
#include "RacerType.h"

#include <string>
#include <vector>
#include <map>

using std::vector;
using std::map;

class Race
{
public:
	Race();
	~Race();

	void addPlayer(RACER_TYPE type);
	void addAI(RACER_TYPE type);
	void removeRacer(int index);
	void startRace();
	void stepFrame();
	void renderFrame();

	bool raceComplete();

	int racerCount();
	vector<Racer*> getRacers();

private:	
	const float OBSTACLE_SLOW_BY_PERCENT = 1.0f;
	RaceDisplayTerminal *display;
	int length = 50;
	vector<Racer*> racers;
	vector<Obstacle*> obstacles;
	map<Racer*, int> lastCollisionPosPerRacer;
	Racer *leader = NULL;

	void resetRace();
	bool collidingWithObstacle(Racer* racer);
	void initLastCollisionPosPerRacer();
	void resetAnnouncements();
};

