/*********************************************************
*
* Title: RaceDisplay.h
* Author: ACBR Team
* Description: Header file for the Abstract RaceDisplay class
* which is intended to be implemented on different platforms
* to handle the rendering of a single race
*
**********************************************************/

#pragma once

#include "Racer.h"
#include "Obstacle.h"

#include <vector>

using std::vector;

class RaceDisplay {
	virtual void clearDisplay() = 0;
	virtual void drawScreen(int length, vector<Racer*> racers, vector<Obstacle*> obstacles) = 0;
	virtual void displayObstacle(Obstacle *obstacle) = 0;
	virtual void displayRacer(Racer *racer) = 0;
	virtual void displayTrack() = 0;
};
