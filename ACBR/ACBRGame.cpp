#include "pch.h"

#include <iostream>
#include <iomanip>
#include <string>

#include "ACBRGame.h"
#include "PressEnterHalt.h"

#include <time.h>

using std::cout;
using std::cin;
using std::flush;
using std::endl;

ACBRGame::ACBRGame()
{
	borderString = string(75, '=');

	srand(time(NULL));

	race = new Race();

	// Setup racers beforehand for easier debugging
	race->addAI(HORSE);
	race->addAI(CAR);
	race->addAI(SPACESHIP);
	race->addAI(AIRPLANE);
}


ACBRGame::~ACBRGame()
{
}

// Print out the main menu
void ACBRGame::printMenu() {
	system("CLS");
	cout << "ACBR Main Menu" << endl << endl;

	cout << borderString << endl << endl;
	printRacerInfo();
	cout << borderString << endl << endl;

	cout << "	1. Add Player (Coming Soon)" << endl;
	cout << "	2. Add AI" << endl;
	cout << "	3. Remove Player" << endl;
	cout << "	4. Begin Race" << endl;
	cout << "	5. Exit" << endl;
	cout << endl;
	cout << "Option: " << flush;
}

// Main entry point to the game, sets up the screen and manages the menu loop
void ACBRGame::initialize() {
	int selection = -1;
	bool error = false;

	cout << "----------------------------------------------------------------------" << endl;
	cout << "Welcome to ACBR" << endl;
	cout << "Notes: " << endl << endl;

	cout << "Please maximize window and press enter..." << endl;
	cout << "----------------------------------------------------------------------" << endl;
	forceStop();

	while (selection != menu_exit_index) {
		if (error) {
			cout << "Invalid menu selection" << endl;
		}
		printMenu();
		cin >> selection;
		cin.ignore();
		if (!handleMenuSelection(selection)) {
			error = true;
		}
	}
}

// Helper function to display the number and type of each racer in the menu
void ACBRGame::printRacerInfo() {
	vector<Racer*> racers = race->getRacers();
	int printedCount = 0;

	cout << "Players configured for current race: " << endl;

	for (Racer* racer : racers) {
		cout << " " << (printedCount + 1) << ": ";
		cout << racer->getName() << "\n";
		printedCount++;
	}

	while (printedCount < max_racers) {
		cout << " " << (printedCount + 1) << ": [UNASSIGNED]" << endl;
		printedCount++;
	}

	cout << endl;
}

// Handler for selecting AI racers, manages a loop for proper selection
void ACBRGame::handleAddAI() {
	int selection = -1;
	bool error = false;

	if (race->racerCount() == max_racers) {
		cout << "Max racer count reached, please delete racer and try again" << endl;
		cout << "Press Enter" << endl;
		forceStop();
		return;
	} 

	system("CLS");
	while (selection < 0 || error) {
		if (error) {
			system("CLS");
			error = false;
			cout << "Invalid selection" << endl;
		}
		cout << "Please select a racer type to add" << endl << endl;
		cout << "	1. Horse:" << endl;
		// Print some stats here?
		cout << endl << endl;
		cout << "	2. Car:" << endl;
		// Print some stats here?
		cout << endl << endl;
		cout << "	3. Spaceship:" << endl;
		// Print some stats here?
		cout << endl << endl;
		cout << "	4. Airplane:" << endl;
		// Print some stats here?
		cout << endl << endl;
		cout << "Please enter your selection: " << flush;
		cin >> selection;
		cin.ignore();
		if (selection < 0 || selection > racer_type_count) {
			error = true;
		}
	}

	if (selection == 1) {
		race->addAI(HORSE);
	}
	else if (selection == 2) {
		race->addAI(CAR);
	}
	else if (selection == 3) {
		race->addAI(SPACESHIP);
	}
	else if (selection == 4) {
		race->addAI(AIRPLANE);
	}
}

// This allows the user to remove a racer, and manages a loop for valid selection
void ACBRGame::handleRacerRemoval() {
	vector<Racer*> racers = race->getRacers();
    int selection = -1;
	bool error = false;

	if (racers.size() == 0) {
		cout << "No racers to delete" << endl;
		cout << "Press Enter" << endl;
		forceStop();
		return;
	} 

	system("CLS");
	while (selection < 0 || error) {
		if (error) {
			system("CLS");
			error = false;
			cout << "Invalid selection" << endl;
		}
		cout << "Which player would you like to remove?" << endl << endl;
		for (int i = 0; i < racers.size(); ++i) {
			cout << " " << i + 1 << ": ";
			cout << racers[i]->getName() << "\n";
		}
		cout << endl << endl;
		cout << "Please enter your selection: " << flush;
		cin >> selection;
		cin.ignore();
		if (selection < 0 || selection > racers.size()) {
			error = true;
		}
	}

	race->removeRacer(selection-1);
}

bool ACBRGame::handleMenuSelection(int in) {
	switch (in) {
	case 1:
		//handlePlayerEntry();
		return true;
	case 2:
		handleAddAI();
		return true;
	case 3:
		handleRacerRemoval();
		return true;	
	case 4:
		play();
		return true;
	default:
		return false;
	}
}

// Entry point for starting a race and entering the main game loop
void ACBRGame::play() {
	system("CLS");

	race->startRace();

	gameLoop();
}

// The game loop is the bread and butter of the operation, looping through players until an end game
// scenario is reached
void ACBRGame::gameLoop() {
	clock_t lastFrameUpdate;
	lastFrameUpdate = clock();
	float fps = 30.0;

	while (!race->raceComplete()) {
		if (clock() - lastFrameUpdate > (1000.0/fps)) {
			//frame
			race->stepFrame();
			race->renderFrame();
		}
		else {
			// Haven't hit the frame timer yet
			continue;
		}
	}

	cout << endl << "Race complete!" << endl;
	cout << endl << "Press Enter to Return to Main Menu" << endl;
	forceStop();
}


