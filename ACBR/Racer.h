/*********************************************************
*
* Title: Racer.h
* Author: ACBR Team
* Description: Header file for the Racer class, which manages
* the type of Racer for the current instance, as well as its
* state in the current race instance
*
**********************************************************/

#pragma once

#include "RacerType.h"
#include <string>
#include "RacetrackPos.h"

using std::string;

class Racer
{
public:
	Racer(RACER_TYPE type);
	~Racer();

	RACER_TYPE getType();
	RacetrackPos* getPos();
	void setPos(float posIn);

	void stepFrame();
	virtual string getName() = 0;
	void slowByPercent(float value);
	virtual bool hasEngine() = 0;

protected:
	float acceleration;
	float maxSpeed;

private:
	RACER_TYPE type;

	RacetrackPos* pos;
	float speed;
};

