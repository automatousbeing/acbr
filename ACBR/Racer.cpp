#include "pch.h"
#include "Racer.h"

#include <random>

Racer::Racer(RACER_TYPE typeIn)
{
	type = typeIn;

	pos = new RacetrackPos();
	speed = 0.0;
}

Racer::~Racer()
{
}

RACER_TYPE Racer::getType() {
	return type;
}

RacetrackPos* Racer::getPos() {
	return pos;
}

// Set the racer's position given a distance from the starting line.
void Racer::setPos(float posIn) {
	pos->setDistFromStart(posIn);
}

// Move the racer
void Racer::stepFrame() {
	*pos += speed;
	speed += acceleration;
	if (speed > maxSpeed)
	{
		speed = maxSpeed;
	}
}

void Racer::slowByPercent(float value)
{
	speed *= (1 - value);
}