#include "pch.h"
#include "PressEnterHalt.h"
#include <iostream>

using std::cout;
using std::flush;
using std::cin;

void forceStop() {
	char emptyChar = ' ';
	char emptyLine[256];
	int count = 0;

	cin.getline(emptyLine, 256);
	count = cin.gcount();

	cout << (char)10;
}