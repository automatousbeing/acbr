#include "pch.h"
#include "RacerFactory.h"
#include "Airplane.h"
#include "Car.h"
#include "Horse.h"
#include "Spaceship.h"


// Create an instance of a Racer based on a given type.
Racer* RacerFactory::makeRacer(RACER_TYPE type) {
	switch (type)
	{
		case RACER_TYPE::AIRPLANE: return new Airplane();
		case RACER_TYPE::CAR: return new Car();
		case RACER_TYPE::HORSE: return new Horse();
		case RACER_TYPE::SPACESHIP: return new Spaceship();
	}
}