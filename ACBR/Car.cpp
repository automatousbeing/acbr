#include "pch.h"
#include "Car.h"
#include "RacerType.h"

// Define the type and stats.
Car::Car() : Racer(RACER_TYPE::CAR) {
	this->maxSpeed = 0.56f;
	this->acceleration = 0.0224f;
}

// Get a name which can be displayed to the user.
string Car::getName() {
	return "Car";
}

// Return whether or not this racer has an engine.
bool Car::hasEngine() {
	return true;
}