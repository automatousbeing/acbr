#include "pch.h"
#include "Spaceship.h"
#include "RacerType.h"

// Define the type and stats.
Spaceship::Spaceship() : Racer(RACER_TYPE::SPACESHIP) {
	this->maxSpeed = 0.8f;
	this->acceleration = 0.014f;
}

// Get a name which can be displayed to the user.
string Spaceship::getName() {
	return "Spaceship";
}

// Return whether or not this racer has an engine.
bool Spaceship::hasEngine() {
	return true;
}