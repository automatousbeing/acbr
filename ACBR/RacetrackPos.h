/*********************************************************
*
* Title: RacetrackPos.h
* Author: ACBR Team
* Description: Header file for the Race class which defines
* an objects position on a one-dimensional race track based
* on its distance from the starting line.
*
**********************************************************/

#pragma once

class RacetrackPos
{
public:
	RacetrackPos();
	float getDistFromStart();
	void setDistFromStart(float value);
	int getRoundedDistFromStart();
	bool equals(RacetrackPos* other);
	RacetrackPos& operator +=(float rhs);

private:
	float distFromStart;
};