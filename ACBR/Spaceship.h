/*********************************************************
*
* Title: Spaceship.h
* Author: ACBR Team
* Description: Header file for the Spaceship class which is
* is a Racer, so it is a playable character. It has poor
* acceleration but excellent max speed.
*
**********************************************************/

#pragma once

#include "Racer.h"
#include <string>

using std::string;

class Spaceship : public Racer
{
public:
	Spaceship();
	string getName();
	bool hasEngine();
};