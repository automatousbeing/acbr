/*********************************************************
*
* Title: Horse.h
* Author: ACBR Team
* Description: Header file for the Horse class which is
* is a Racer, so it is a playable character. It has poor
* max speed but excellent acceleration.
*
**********************************************************/

#pragma once

#include "Racer.h"
#include <string>

using std::string;

class Horse : public Racer
{
public:
	Horse();
	string getName();
	bool hasEngine();
};