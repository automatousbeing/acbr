/*********************************************************
*
* Title: RacerType.h
* Author: ACBR Team
* Description: Defines the different types of racers.
*
**********************************************************/


#pragma once

enum RACER_TYPE {
	HORSE, CAR, SPACESHIP, AIRPLANE
};

